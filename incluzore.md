# Objectif du projet 
## __Créer une convertisseuse en écriture inclusive__


# <img src="images/incluzore.png" alt="page d'accueil d'Incluzore" style="width:50%;height: auto;" >

{pause up-at-unpause}
# Contexte du projet
C'est lors d'un hackathon sur l'écriture inclusive en 2018, que ce projet a pu se concrétiser.
son est apparu pour la première fois lors de la rédaction de post de la page Facebook du mouvement Nuit Debout.
En effet certain rédacteurices ne savaient pas quelles règles appliquer pour représenter de manières inclusives les acteurices des initiative.

## Objectif final
- Pouvoir rendre automatiquement un texte en langage inclusif.

### intérêts
- Scientifique : mesurer la représentation des femmes dans les représentations littéraires et la replacer dans des contextes historiques
- Historique : Créer une histoire des luttes des représentations de genres
- Pédagogique : Transmettre une histoires des luttes de genres.
- Informatique : Créer des LLM qui identifient et classifie plus précisément les contenus littéraires.
- Marketing : Créer un meilleur taux d'engagement pour les personnes qui se reconnaissent dans les contenus produits.


{pause up-at-unpause}
### Besoins

- Méthodologies de construction des LLM
- Financer une ou plusieurs personnes pour permettre la finalisation du projet

{pause up-at-unpause}
## Comment peut on vous aider ?
Envoyez nous un mail à contact@incluzor.fr quelque soit votre investissement, nous avons toujours besoin d'aide. Que ce soit pour nous structurer, pour améliorer nos outils, pour communiquer. Bref vous êtes le ou la bienvenue !




{pause up-at-unpause}
# Comprendre le projet

## À propos
IncluZor·e s’inscrit dans le contexte sociétal actuel, qui semble enfin capable d’accorder les droits d’expression et de représentation aux femmes et aux personnes ne se reconnaissant pas dans la binarité des genres féminin et masculin.

{pause up-at-unpause}
## Comment s'en servir ? 
Ce site et cet outil visent à fournir à toute personne souhaitant convertir un texte en français inclusif les moyens de le faire de la façon la plus pertinente et adéquate pour elle. La convertisseuse IncluZor·e s’adresse à toutes celles et ceux qui souhaitent nommer les femmes à part égale dans leurs discours et leurs écrits. Elle s’adresse aussi à toustes celleux qui veulent s’exprimer dans une langue non strictement binaire, c’est-à-dire dans une langue qui reflète des positions générées plus complexes et/ou plus fluides. La convertisseuse met à la disposition de toustes plusieurs procédés d’inclusion — des doublets féminin masculin (lectrices et lecteurs) aux néologismes (lecteurices), dont des neutres (lectaires).


<img src="public/images/convertisseuse-incluzore.png" alt="page d'accueil d'Incluzore" style="width:70%;height: auto;align=center">

{pause up-at-unpause}

## Comment son produit les conversion ? 
Le projet IncluZor·e dispose d’une pertinence linguistique : en se fondant sur les usages actuels et les travaux de linguistes féministes, queer, non binaires ou agenres, sur les travaux de recherche syntaxique de l’INRIA, il propose des lexiques et des points de grammaire inclusive (tel cet accord en genre et nombre avec le terme le plus proche, le fameux accord de proximité). Il est doté d’une pertinence technique, reposant sur des analyseurs et des algorithmes. Il est le fruit d’un travail collectif, rassemblant des amoureureuses de la langue française et de l’expression sans discrimination. Composée d’informatic·ien·cienne·s, de webdesigners, de linguistes, d’une ancienne lexicographe, de rédactrice·teurs, de journalistes et d’étudiantEs en études de genre, l'équipe IncluZor·e est bénévole.

Rejoignez le mouvement IncluZor·e !

{pause up-at-unpause}
### Écritures Inclusives : Détails et Études Scientifiques

L'écriture inclusive est une pratique qui vise à rendre la langue plus égalitaire en tenant compte de la diversité des genres. Plusieurs formes d'écriture inclusive existent, chacune ayant ses propres règles et applications. Voici un aperçu détaillé des différentes formes d'écriture inclusive, accompagné d'exemples et des études scientifiques qui en évaluent l'utilité.


{pause up-at-unpause}
#### 1. Le point médian
{.definition title="Le point médian"}
Le point médian (·) est utilisé pour inclure à la fois les formes masculines et féminines dans un mot. Par exemple :

{.example title="Exemple"}
- Étudiant·e·s au lieu de "étudiants" pour inclure les étudiantes.
- Ami·e·s au lieu de "amis" pour inclure les amies.



{.theorem title="Études scientifiques"}
- **Étude de l'Académie Française (2017)** : L'Académie Française a exprimé des réserves concernant le point médian, soulignant que cette pratique complexifie la lecture et l'écriture.
- **Étude de Prewitt-Freilino et al. (2012)** : Cette étude a examiné l'impact des langues genrées sur les attitudes de genre et a suggéré que des langues avec des options plus inclusives peuvent influencer positivement les attitudes envers l'égalité des genres.



{pause up-at-unpause}
#### 2. L'accord de proximité
{.definition title="L'accord de proximité"}
L'accord de proximité consiste à accorder l'adjectif ou le participe avec le nom le plus proche, indépendamment de son genre grammatical.

{.example title="Exemple"}
- "Les hommes et les femmes sont belles" au lieu de "Les hommes et les femmes sont beaux".

{.theorem title="Études scientifiques"}
- **Étude de Nissen (2002)** : Cette étude a montré que l'accord de proximité peut faciliter la lecture en rendant les phrases plus intuitives, mais elle souligne également que cette pratique peut entrer en conflit avec les règles grammaticales traditionnelles.

{pause up-at-unpause}
#### 3. La féminisation des titres
{.definition title="La féminisation des titres"}
La féminisation des titres et des fonctions consiste à utiliser des formes féminines pour les titres et les fonctions professionnelles.

{.example title="Exemple"}
- "Autrice" au lieu de "auteur" pour une femme.
- "Ingénieure" au lieu de "ingénieur" pour une femme.

{.theorem title="Études scientifiques"}
- **Étude de Merkel, Maass et Frommelt (2012)** : Cette recherche a démontré que la féminisation des titres contribue à une meilleure visibilité des femmes dans les domaines professionnels et peut influencer positivement les aspirations professionnelles des jeunes filles.


{pause up-at-unpause}
#### 4. Les pronoms non binaires
{.definition title=" Les pronoms non binaires"}
Certains pronoms neutres sont introduits pour inclure les personnes non-binaires. Par exemple :

{.example title="Exemples"}
- "Iel" au lieu de "il" ou "elle".
- "Elleux" au lieu de "eux" ou "elles".

{.theorem title="Études scientifiques"}
- **Étude de Fiske et Taylor (1991)** : Cette étude sur la cognition sociale suggère que l'utilisation de pronoms neutres peut aider à réduire les stéréotypes de genre en langage.

{pause up-at-unpause}
#### 5. L'écriture épicène
{.definition title=" L'écriture épicène"}
L'écriture épicène consiste à utiliser des termes non marqués par le genre ou à reformuler les phrases pour éviter l'utilisation du genre.

{.example title="Exemples"}
- "Le personnel" au lieu de "les employés" ou "les employées".
- "La personne responsable" au lieu de "le responsable" ou "la responsable".

{.theorem title="Études scientifiques"}
- **Étude de Gygax et Gabriel (2008)** : Cette étude a montré que l'écriture épicène peut réduire les biais de genre en langue et est généralement plus facile à lire et à comprendre que l'utilisation du point médian.

{pause up-at-unpause}
### Conclusion
L'écriture inclusive vise à promouvoir l'égalité des genres en langue, bien que son utilité et son acceptation varient selon les contextes et les individus. Les études scientifiques montrent des avantages potentiels en termes de réduction des biais de genre et de promotion de l'égalité, mais soulignent également des défis en termes de complexité et d'acceptation par les utilisateurs.

{pause up-at-unpause}
{.theorem title="Études scientifiques"}
Pour plus de détails, voici quelques études de référence :
- **Prewitt-Freilino, J. L., Caswell, T. A., & Laakso, E. K. (2012). The Gendering of Language: A Comparison of Gender Equality in Countries with Gendered, Natural Gender, and Genderless Languages.** Sex Roles, 66(3-4), 268-281.
- **Nissen, U. K. (2002). Gender in translation: Cultural identity and the politics of transmission.** In H. S. Parshley (Ed.), Gender and language (pp. 103-120).
- **Merkel, E., Maass, A., & Frommelt, L. (2012). The Impact of Gender-Fair Language on Social Cognition: The Case of German.** European Journal of Social Psychology, 42(2), 210-218.
- **Gygax, P., & Gabriel, U. (2008). Can a Group of Musicians be Composed of Women? Generic Interpretation of French Masculine Role Names in Comparison with German.** Swiss Journal of Psychology, 67(3), 143-151.


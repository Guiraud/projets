# Contexte du projet

Le projet Whistlebox s'inscrit dans une initiative visant à renforcer la protection des lanceurs d'alerte en utilisant les technologies de l'information et de la communication. Les lanceurs d'alerte jouent un rôle crucial dans la transparence et la responsabilité des institutions, mais ils sont souvent exposés à des risques personnels et professionnels. Whistlebox vise à fournir un outil sécurisé et anonyme pour faciliter la dénonciation d'actes répréhensibles tout en protégeant l'identité des lanceurs d'alerte.

# ![WistleBox](images/whistleBox.png)

{pause up-at-unpause}
# Objectif du projet

L'objectif principal de Whistlebox est de créer une plateforme sécurisée permettant aux lanceurs d'alerte de soumettre des informations de manière anonyme et protégée. Cette plateforme doit :

- Garantir l'anonymat des utilisateurs.
- Faciliter la transmission sécurisée d'informations sensibles.
- Assurer une communication sécurisée entre les lanceurs d'alerte et les parties prenantes.
- Promouvoir la transparence et la responsabilité au sein des organisations.

{pause up-at-unpause}
#  Intérêts
## Scientifique

Le projet Whistlebox offre des opportunités uniques pour la recherche en cybersécurité et en protection des données. Il permet d'explorer les meilleures pratiques en matière de cryptographie et de sécurité des communications pour protéger les informations sensibles transmises par les lanceurs d'alerte.

{pause up-at-unpause}
## Historique

Historiquement, les lanceurs d'alerte ont joué un rôle crucial dans la révélation de scandales et d'abus de pouvoir. Whistlebox s'inscrit dans cette tradition en fournissant un outil moderne qui répond aux défis actuels de la protection des lanceurs d'alerte.

{pause up-at-unpause}
## Pédagogique

Whistlebox peut servir de ressource pédagogique pour les institutions académiques, en enseignant aux étudiants les enjeux éthiques et technologiques liés à la protection des lanceurs d'alerte. Il peut également sensibiliser le public à l'importance de la transparence et de la responsabilité dans les organisations.

{pause up-at-unpause}
## Informatique

Pour les professionnels de l'informatique, Whistlebox présente un défi technique passionnant en matière de sécurisation des données et de développement de plateformes résilientes. Il offre une opportunité de travailler sur des technologies avancées de cryptographie et de sécurité de l'information. 
Le projet s'appuie sur les technologies suivantes : 
- TOR : pour le service de serveur hébergé
- OpenPGP : pour le lien entre la box et les acteurices

{pause up-at-unpause}
## Marketing

Du point de vue marketing, Whistlebox peut améliorer la confiance du public dans les organisations qui l'adoptent. En garantissant la protection des lanceurs d'alerte, les entreprises et les institutions peuvent renforcer leur image de transparence et de responsabilité, ce qui peut accroître l'engagement et la fidélité des clients et des employés.


{pause up-at-unpause}
# Besoins

- Recherche et développement : Investissement dans la recherche en cybersécurité et en cryptographie pour garantir la sécurité de la plateforme.
- Partenariats : Collaboration avec des organisations de défense des droits humains et des experts en protection des lanceurs d'alerte.
- Formation : Programmes de formation pour les utilisateurs de la plateforme, ainsi que pour les administrateurs et les responsables de la sécurité.
- Financement : Ressources financières pour le développement, la maintenance et l'amélioration continue de la plateforme.

{pause up-at-unpause}
# Comment peut-on vous aider

- Experts : Contributions d'experts en cybersécurité, en droit, et en protection des lanceurs d'alerte.
- Organisations : Soutien des organisations non gouvernementales et des institutions publiques pour promouvoir et adopter Whistlebox.
- Financement : Aide financière pour soutenir les phases de développement et de déploiement de la plateforme.
- Partenariats : Collaboration avec des universités, des entreprises technologiques et des organismes de réglementation pour développer des solutions innovantes et efficaces.

{pause focus-at-unpause}
# Comprendre le projet

Whistlebox est conçu pour être une solution complète de protection des lanceurs d'alerte, en intégrant des technologies de pointe en matière de sécurité et de cryptographie. La plateforme permet une communication anonyme et sécurisée, tout en assurant la confidentialité des informations transmises. En définissant des standards élevés de protection des données, Whistlebox vise à encourager davantage de personnes à signaler des actes répréhensibles sans crainte de représailles.

{pause center-at-unpause}
# Journalisme et intelligence artificielle : détails et études scientifiques


L'utilisation de l'IA dans le cadre de Whistlebox pourrait inclure des fonctions avancées de détection de menaces, d'analyse de données pour identifier des schémas de comportements suspects, et de filtrage intelligent des informations soumises. Des études scientifiques montrent que l'IA peut améliorer la sécurité et l'efficacité des plateformes de signalement, mais soulignent également l'importance de la supervision humaine pour garantir la précision et l'intégrité des résultats.

{pause center-at-unpause}
# Conclusion

Whistlebox représente une avancée significative dans la protection des lanceurs d'alerte, en combinant des technologies de pointe avec un engagement ferme en faveur de la transparence et de la responsabilité. En fournissant une plateforme sécurisée et anonyme, Whistlebox encourage la dénonciation d'actes répréhensibles, renforçant ainsi la confiance du public dans les institutions et contribuant à un environnement plus juste et transparent. Le succès de ce projet dépendra de la collaboration entre les experts, les organisations et les communautés concernées, ainsi que du soutien financier et technique nécessaire pour garantir son déploiement et son évolution continue.



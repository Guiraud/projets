# La roulette de mes projets

Cliquez sur [la roulette](https://Guiraud.gitlab.io/projets)

## Description

Ce site web présente une roue de casino interactive qui permet aux utilisateurs de sélectionner aléatoirement une page parmi cinq options différentes. Lorsque la roue s'arrête, une image générée par DALL-E 3 est affichée, représentant la page sélectionnée. L'utilisateur peut alors cliquer sur l'image pour accéder à la page respective.

## Fonctionnalités

- **Roue de Casino Interactive** : La roue tourne lorsque l'utilisateur clique sur le bouton "Lancer la roue" et s'arrête sur une section aléatoire.
- **Images Générées par DALL-E 3** : Chaque section de la roue correspond à une image générée par DALL-E 3, affichée lorsque la roue s'arrête.
- **Redirection par Clic** : En cliquant sur l'image affichée, l'utilisateur est redirigé vers une des cinq pages distinctes.

## Pages Disponibles

1. **Incluzore** : Une convertisseuse en langage inclusif.
   - ![Incluzore](images/incluzore.webp)

2. **Charte de Munich AI** : Une charte pour les journalistes afin d'adapter le journalisme à la révolution de l'usage de l'intelligence artificielle.
   - ![Charte de Munich AI](images/ChartedeMunichAI.webp)

3. **Open-Tarif.xyz** : Un site pour consulter les évolutions des prix de l'électricité.
   - ![Open-Tarif.xyz](images/Open-Tarif.webp)

4. **Instruire.ai** : Un site qui permet l'auto-apprentissage de tous les savoirs. Une plateforme d'échange.
   - ![Instruire.ai](images/instruire_ai.webp)

5. **Causeries populaires** : Un média d'éducation populaire au débat et à la décision collective.
   - ![Causeries populaires](images/causeriespopulaires.webp)

## Installation et Déploiement

Pour installer et déployer ce site sur GitLab Pages, suivez les étapes ci-dessous :

1. **Clonez le dépôt** :
    ```sh
    git clone <URL-du-dépôt>
    cd <nom-du-répertoire>
    ```

2. **Créez les fichiers HTML pour chaque page** dans le dossier `public`.

3. **Assurez-vous que les images générées par DALL-E 3** sont placées dans le dossier `public/images`.

4. **Configurez le fichier `.gitlab-ci.yml`** :
    ```yaml
    image: node:latest

    stages:
      - build
      - deploy

    pages:
      stage: deploy
      script:
        - mkdir -p public
        - shopt -s extglob
        - cp -r !(public) public/
      artifacts:
        paths:
          - public
      only:
        - main
      publish: public
    ```

5. **Commitez et poussez les changements** :
    ```sh
    git add .
    git commit -m "Initial commit with casino wheel website"
    git push origin main
    ```

## Utilisation

- **Lancer la roue** : Cliquez sur le bouton "Lancer la roue" pour démarrer la rotation.
- **Voir le résultat** : Une image correspondant à une des cinq pages sera affichée une fois la roue arrêtée.
- **Accéder à la page** : Cliquez sur l'image pour être redirigé vers la page respective.

## Contributions

Les contributions sont les bienvenues. Veuillez soumettre une pull request avec une description détaillée des modifications.

## Licence

Ce projet est sous licence MIT. Voir le fichier [LICENSE](LICENSE) pour plus de détails.
